std::vector<int> naive_multiplication(std::vector<int>&d1,std::vector<int>&d2);
void carry_res(std::vector<int>&res);
std::vector<int> sum(std::vector<int>&d1, std::vector<int>&d2);
std::vector<int> diff(std::vector<int>&d1, std::vector<int>&d2);
std::vector<int> karatsuba_mul(std::vector<int>&d1, std::vector<int>&d2);